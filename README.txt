
Summary
-------
When sending e-mail / using CDN/static file server, we
often need to parse images to absolute url. This is why
this tiny module exists.

This module only parse relative url to absolute url 
dynamiclly, and you can also set custom url when you have
CDN/static file deliver server.


Installation
------------
To install this module, do the following:

1. copy to module directory (modules/  or sites/all/modules).
3. after module enabled, check out settings page to enable this function. (admin/settings/abssrc)

Bugs/Features/Patches
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)

If you use this module, find it useful, and want to send the author
a thank you note, feel free to contact me.

The author can also be contacted for paid customizations of this
and other modules.

